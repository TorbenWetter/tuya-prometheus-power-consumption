## Prerequisites
- Install [Grafana](grafana.com) (default port 3000)
- Install [Prometheus](prometheus.io) (default port 9090)
- Create a Cloud Project on [Tuya's IoT Platform](iot.tuya.com)
- Go to "Device Management" -> "Linked Device" -> "Link Devices by App Account" to link your TuyaSmart account to the Cloud project
- Use the [Tuya CLI Client](https://github.com/TuyaAPI/cli) to determine the local IDs and keys of the smart sockets and write them to sockets.json in the following format:
```
[
    {
        "name": "Example Device Name",
        "id": "Example Device ID",
        "key": "Example Device Key",
    },
    ...
]
```

## Installing the npm packages
```
npm install
```

## Running the server
```
npm start
```

## Preparing Prometheus
- Add 'localhost:[Port]' to scrape_configs -> targets in prometheus/prometheus.yml

## Preparing Grafana
- Select Prometheus as data source (use the default port 9090)
- Create a new dashboard and select the desired metrics from Prometheus