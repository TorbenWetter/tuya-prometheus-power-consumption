const fs = require('fs');
const TuyAPI = require('tuyapi');
const Prometheus = require('prom-client');
const app = require('express')();
const ON_DEATH = require('death');

// Read the sockets file (name, id and key)
const sockets = JSON.parse(fs.readFileSync('sockets.json'));

// Create the device objects using the TuyAPI
const devices = sockets.map((socket) => new TuyAPI({ id: socket.id, key: socket.key }));

// Loop through the devices
for (const [i, device] of devices.entries()) {
  const deviceName = sockets[i].name;

  // Create a Prometheus Gauge (https://prometheus.io/docs/concepts/metric_types) to track the power consumption
  const gauge = new Prometheus.Gauge({
    name: `${deviceName.toLowerCase()}_power_consumption_watts`,
    help: `Power consumption of ${deviceName} in watts`,
  });

  // Connect to the device
  console.log(`Connecting to ${deviceName}...`);
  device.find().then(() => device.connect());

  device.on('connected', () => {
    console.log(`Connected to ${deviceName}.`);
  });

  device.on('disconnected', () => {
    console.log(`Disconnected from ${deviceName}.`);
    // Reconnect to the device
    console.log(`Reconnecting to ${deviceName}...`);
    device.find().then(() => device.connect());
  });

  device.on('error', (error) => {
    console.log(`Error with ${deviceName}:`, error);
  });

  // Listen for data changes
  device.on('data', (data) => {
    // Retrieve the data from dps object (numbered values)
    const on = data.dps['1'];
    // let A = data.dps['18']; // has to be divided by 1000
    let W = data.dps['19'];
    // let V = data.dps['20']; // has to be divided by 10

    // If the device's "power-on-status" has changed
    if (on !== undefined) {
      console.log(`${deviceName} was turned ${on ? 'on' : 'off'}.`);
    }

    // If the watts value has changed
    if (W !== undefined) {
      // Has to be divided by 10
      W /= 10;
      gauge.set(W);
      console.log(`New data for ${deviceName}: ${W} W`);
    }
  });
}

const port = 9095;

// Answer requests from the Prometheus client
app.get('/metrics', (req, res) => {
  res.set('Content-Type', Prometheus.register.contentType);
  res.end(Prometheus.register.metrics());
});

const server = app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`);
});

// Disconnect from the devices and close the server on program exit
ON_DEATH((signal, err) => {
  for (let device of devices) {
    device.disconnect();
  }
  server.close();
});
